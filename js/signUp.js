$(document).ready(function () {
	$("#sign-up-button").click(function (event) {
		event.preventDefault();

		var email = $("#sign-up-email").val();
		var password = $("#sign-up-password").val();

		firebase.auth().createUserWithEmailAndPassword(email, password)
			.then(function () {

				window.location = "updateProfile.html"
			})
			.catch(function (error) {
				// Handle Errors here.
				var errorCode = error.code;
				var errorMessage = error.message;
				alert(errorMessage);
			});
	});
});