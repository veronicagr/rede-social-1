$(document).ready(function () {

	firebase.auth().onAuthStateChanged(function (user) {
		if (!user) {
			window.location = "signIn.html"
		}
	});

	$("#sign-up-button").click(function (event) {
		event.preventDefault();

		var name = $("#sign-up-name").val();
		var phone = $("#sign-up-phone").val();

		var user = firebase.auth().currentUser;

		user.updateProfile({
			displayName: name,
			phoneNumber: phone,
			photoURL: "http://placekitten.com/g/200/300"
		}).then(function () {

			window.location = "index.html"
		}).catch(function (error) {
			// An error happened.
			var errorCode = error.code;
			var errorMessage = error.message;
			alert(errorMessage);
		});
	});
});