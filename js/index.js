var userLogged;
$(document).ready(function () {

	firebase.auth().onAuthStateChanged(function (user) {
		if (!user) {
			window.location = "signIn.html"
		} else {
			userLogged = user;
		}
	});
	console.log(userLogged);

	$(".image").append("<img src='http://placekitten.com/g/200/300'>");

	$("#logout-button").click(function (event) {
		event.preventDefault();

		firebase.auth().signOut()
			.then(function () {
				window.location = "signIn.html"
			})
			.catch(function (error) {
				console.log(error);
			});

	});
});