var database = firebase.database();
var USER_ID = window.location.search.match(/\?id=(.*)/)[1];


$(document).ready(function () {

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          console.log(user);
        } else {
          window.location = 'index.html';
        }
      });

    database.ref("/task/" + USER_ID).once('value')
        .then(function (snapshot) {

            snapshot.forEach(function (childSnapshot) {

                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();

                $(".task-list").append(`
                <li>
                <input type= "checkbox" data-task-id=${childKey} />
                <span><textarea>${childData.text}</textarea></span>
                </li>`);

                $(`input[data-task-id="${childKey}"]`).click(function () {
                    database.ref("task/" + USER_ID + "/" + childKey).remove();
                    $(this).parent().remove();
                });
            });

        });

    $(".add-task").click(function (event) {
        event.preventDefault();

        var newTask = $(".task-input").val();

        var taskFromDB = database.ref("task/" + USER_ID).push({
            text: newTask
        });
        $(".task-list").append(`
        
        <li>
        <input type = "checkbox" data-task-id=${taskFromDB.key} />
        <span><textarea>${newTask}</textarea></span>
        </li>`);

        $(`input[data-task-id='${taskFromDB.key}']`).click(function () {
            database.ref("task/" + USER_ID + "/" + taskFromDB.key).remove();
            $(this).parent().remove();
        })
    });

});